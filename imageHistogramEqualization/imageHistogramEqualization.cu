#include <cuda.h>
#include <nvToolsExt.h>
#include <cstdio>
#include <opencv2/opencv.hpp>
#include <vector>
#include <ctime>
#include <fstream>

using namespace cv;
using namespace std;
__global__ void histogramEqualization(
		uchar *inputData,
		uchar *outputData,
		uchar *colorMap,
		size_t width,
		size_t height,
		int pixelsPerThread)
{
	size_t idx = blockIdx.x*blockDim.x+threadIdx.x;
	size_t pixelStart = pixelsPerThread*idx;
	if(pixelStart>=width*height) return;
	size_t pixelEnd =
			min(width*height,pixelsPerThread*(idx+1));
	for(int i=pixelStart;i<pixelEnd;i++)
		outputData[i]=colorMap[inputData[i]];
}
__global__ void fillHistogramGrayscaleImage(uchar *imageGrayscale, int *g_histogram, size_t width, size_t height, int pixelsPerThread)
{
	//blockDim.x must be equal to 256
	/*
	 * Global memory access: pixelsPerThread+1
	 * Calculations: 5 products, pixelsPerThread+1 add
	 */
	__shared__ int b_histogram[256];
	b_histogram[threadIdx.x] = 0;
	__syncthreads();
	size_t idx = blockIdx.x*blockDim.x+threadIdx.x;
	size_t pixelStart = pixelsPerThread*idx;
	if(pixelStart>=width*height) return;
	size_t pixelEnd =
			min(width*height,pixelsPerThread*(idx+1));
	for(int i=pixelStart;i<pixelEnd;i++)
		atomicAdd(&b_histogram[imageGrayscale[i]],1);
	__syncthreads();
	atomicAdd(&g_histogram[threadIdx.x],b_histogram[threadIdx.x]);
}

int main(int argc, char **argv)
{
	if(argc!=2)
	{
		printf("[INFO] Usage: %s image.jpg\n",argv[0]);
		exit(0);
	}
	Mat imageGrayscale;
	imageGrayscale = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
	if(!imageGrayscale.data)
	{
		printf("[ERR] Couldn't open %s\n",argv[1]);
		exit(0);
	}
	namedWindow("Unequalized image", WINDOW_AUTOSIZE);
	imshow("Unequalized image",imageGrayscale);
	int width=imageGrayscale.cols,height=imageGrayscale.rows;
	size_t imageBytesCount = sizeof(uchar)*width*height;
	int *h_histogram,*d_histogram;
	h_histogram=(int*)malloc(sizeof(int)*256);
	cudaMalloc((void **)&d_histogram,sizeof(int)*256);
	cudaMemset(d_histogram,0,sizeof(int)*256);
	uchar *d_imageGrayscaleData;
	cudaMalloc((void **)&d_imageGrayscaleData,imageBytesCount);
	cudaMemcpy(d_imageGrayscaleData,
			imageGrayscale.data,
			imageBytesCount,
			cudaMemcpyHostToDevice);

	int pixelsPerThread = 1024;
	dim3 blockSize(256);
	dim3 gridSize(
			ceil(
				(float)height*(float)width/(float)blockSize.x/(float)pixelsPerThread));
	fillHistogramGrayscaleImage<<<gridSize,blockSize>>>(
			d_imageGrayscaleData,
			d_histogram,
			width,
			height,
			pixelsPerThread
	);
	cudaDeviceSynchronize();
	cudaMemcpy(h_histogram,d_histogram,
			sizeof(int)*256,
			cudaMemcpyDeviceToHost);
	ofstream ficheroSalidaHandle;
	ficheroSalidaHandle.open("histogram.csv");
	ficheroSalidaHandle << "level,n" << endl;
	for(int i=0;i<256;i++)
		ficheroSalidaHandle << i << "," <<
				h_histogram[i] << endl;
	ficheroSalidaHandle.close();

	float h_relFrequenciesHistogram[256];
	for(int i=0;i<256;i++)
		h_relFrequenciesHistogram[i]=
				(float)h_histogram[i]/((float)height*(float)width);
	uchar h_relationship[256];
	float relFrequencyAccum = 0.0;
	for(int i=0;i<256;i++)
	{
		relFrequencyAccum+=
				h_relFrequenciesHistogram[i];
		h_relationship[i]=floor(255.0*relFrequencyAccum);
	}
	uchar *d_imageGrayscaleDataEqualized;
	cudaMalloc((void **)&d_imageGrayscaleDataEqualized,imageBytesCount);
	uchar *d_relationship;
	cudaMalloc((void **)&d_relationship,sizeof(uchar)*256);
	cudaMemcpy(d_relationship,h_relationship,
			sizeof(uchar)*256,
			cudaMemcpyHostToDevice);
	cout << gridSize.x << "," << blockSize.x << endl;
	histogramEqualization<<<gridSize,blockSize>>>(
		d_imageGrayscaleData,
		d_imageGrayscaleDataEqualized,
		d_relationship,
		width,
		height,
		pixelsPerThread
	);
	cudaDeviceSynchronize();
	uchar *h_imageGrayscaleDataEqualized;
	h_imageGrayscaleDataEqualized =
			(uchar *)malloc(imageBytesCount);
	cudaMemcpy(h_imageGrayscaleDataEqualized,
			d_imageGrayscaleDataEqualized,
			imageBytesCount,
			cudaMemcpyDeviceToHost);
	Mat equalizedImage(
			height,
			width,
			CV_8U,
			(void *)h_imageGrayscaleDataEqualized);
	namedWindow("Equalized image", WINDOW_AUTOSIZE);
	imshow("Equalized image",equalizedImage);

	waitKey(0);

	free(h_histogram);
	cudaFree(d_histogram);
	cudaFree(d_imageGrayscaleData);
	imageGrayscale.release();
	return 0;
}
