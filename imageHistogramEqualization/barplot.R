bp <- read.csv(file="histogram.csv",header=TRUE,sep=",");
barplot(height=bp$n,
	names.arg=bp$level,
	main="Histogram of grayscale image file");